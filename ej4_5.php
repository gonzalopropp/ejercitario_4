<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ejercicio 5</title>
</head>
<body>
<?php
	// Crea la Conexion
	$conn = pg_connect("host=127.0.0.1 port=5432 dbname=ejercicio4 user=postgres password=arthur2941");

	if(!$conn) 
	{
		echo "Ocurrió un error al conectar";
		exit;
	}
	
	// Borra todos los datos que existen dentro de la tabla 'tabla1' de la BD ejercicio4.
	$resultado= pg_query($conn, "DELETE FROM tabla1");

	// Inserta los 1000 registros, con strings aleatorios. Se usa la funcion GenerarStringRandom.
	for ($i = 0; $i < 1000; $i++)
	{
		$dato_nombre = GenerarStringRandom(40);
		$dato_descripcion = GenerarStringRandom(250);
		$consulta="INSERT INTO tabla1 VALUES($i + 1, '$dato_nombre', '$dato_descripcion')";
		$resultado= pg_query($conn, $consulta);
	}


	// Se consulta los datos de la tabla, para mostrar en HTML
	$resultado = pg_query($conn, "SELECT * from tabla1");

	if (!$resultado)
	{
		echo "Ocurrió un error al consultar";
		exit;
	}

	// Se muestran los datos tabulados.
	echo "<table>";
		echo "<tr>";
			echo "<td style='border:1px solid black; background-color:red;'> ID </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Nombre </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Descripcion </td>";
		echo "</tr>";

	while ($row = pg_fetch_row($resultado))
	{
		echo "<tr>";
			echo "<td style='border:1px solid black;'> $row[0] </td>";
			echo "<td style='border:1px solid black;'> $row[1] </td>";
			echo "<td style='border:1px solid black;'> $row[2] </td>";
		echo "</tr>";
	}
	echo "</table>";

	// Se cierra la conexion.
	pg_close($conn);


function generarStringRandom($largoString) //Funcion para generar strings aleatorios
{
	$caracteres = 'abcdefghijklmnopqrstuvwxyz';
	$largo = $largoString;
	$resultado = "";

	for ($i = 0; $i < $largo; $i++) 
	{
		$resultado .= $caracteres[rand(0, 25)];
	}
	return $resultado;
}
?>
</body>
</html>