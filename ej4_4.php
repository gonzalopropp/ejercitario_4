<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ejercicio 4</title>
</head>
<body>
<?php
try {
	$conn = new PDO('pgsql:host=127.0.0.1;dbname=ejercicio1', 'postgres', 'arthur2941');

	$query = "SELECT p.nombre, p.precio, m.nombre, e.nombre, c.nombre FROM producto p, marca m, empresa e, categoria c WHERE p.id_marca = m.id_marca AND p.id_categoria = c.id_categoria AND m.id_empresa = e.id_empresa";
	$sql = $conn->prepare($query);
	$sql->execute();
	$resultado = $sql->fetchAll();

	echo "<table>";
		echo "<tr>";
			echo "<td style='border:1px solid black; background-color:red;'> Nom. Producto </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Precio</td>";
			echo "<td style='border:1px solid black; background-color:red;'> Marca </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Empresa </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Categoría </td>";
		echo "</tr>";
		
		//print_r($resultado);

		foreach ($resultado as $row)
		{
			echo "<tr>";
			echo "<td style='border:1px solid black;'> {$row['0']} </td>";
			echo "<td style='border:1px solid black;'> {$row['1']} </td>";
			echo "<td style='border:1px solid black;'> {$row['2']} </td>";
			echo "<td style='border:1px solid black;'> {$row['3']} </td>";
			echo "<td style='border:1px solid black;'> {$row['4']} </td>";
			echo "</tr>";
		}
		
	echo "</table>";

	$conn = null;
} catch (PDOException $e) {
	echo "ERROR: " . $e->getMessage();
}

?>
</body>
</html>