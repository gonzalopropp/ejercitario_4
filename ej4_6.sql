-- Añadir la extension pgcrypto
CREATE EXTENSION IF NOT EXISTS pgcrypto;

-- DDL
CREATE TABLE USUARIOS (
   ID                   SERIAL               not null,
   USUARIO              VARCHAR(35)         null,
   PASSWORD             VARCHAR(35)         null,
   CONSTRAINT PK_USUARIOS PRIMARY KEY (ID)
);

INSERT INTO USUARIOS VALUES (1, 'usuario1', crypt('test', gen_salt('md5')));
INSERT INTO USUARIOS VALUES (2, 'usuario2', crypt('test', gen_salt('md5')));


/*
-- encriptar contraseña
WITH encrypted_data AS (
  SELECT crypt('PasswordToEncrypt0',gen_salt('md5')) as hashed_value
)
UPDATE test_encrypt SET value = (SELECT hashed_value FROM encrypted_data);

CREATE TABLE test_encrypt(
  value TEXT
);
INSERT INTO test_encrypt VALUES ('testvalue');

-- encrypt value
WITH encrypted_data AS (
  SELECT crypt('PasswordToEncrypt0',gen_salt('md5')) as hashed_value
)
UPDATE test_encrypt SET value = (SELECT hashed_value FROM encrypted_data);
*/