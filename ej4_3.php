<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ejercicio 3</title>
</head>
<body>
<?php
	$conn = pg_connect("host=127.0.0.1 port=5432 dbname=ejercicio1 user=postgres password=arthur2941");

	if(!$conn) 
	{
		echo "Ocurrió un error al conectar";
		exit;
	}
	$result = pg_query($conn, "SELECT p.nombre, p.precio, m.nombre, e.nombre, c.nombre FROM producto p, marca m, empresa e, categoria c WHERE p.id_marca = m.id_marca AND p.id_categoria = c.id_categoria AND m.id_empresa = e.id_empresa");

	if (!$result)
	{
		echo "Ocurrió un error al consultar";
		exit;
	}

	echo "<table>";
		echo "<tr>";
			echo "<td style='border:1px solid black; background-color:red;'> Nom. Producto </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Precio</td>";
			echo "<td style='border:1px solid black; background-color:red;'> Marca </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Empresa </td>";
			echo "<td style='border:1px solid black; background-color:red;'> Categoría </td>";
		echo "</tr>";

	while ($row = pg_fetch_row($result))
	{
		echo "<tr>";
			echo "<td style='border:1px solid black;'> $row[0] </td>";
			echo "<td style='border:1px solid black;'> $row[1] </td>";
			echo "<td style='border:1px solid black;'> $row[2] </td>";
			echo "<td style='border:1px solid black;'> $row[3] </td>";
			echo "<td style='border:1px solid black;'> $row[4] </td>";
		echo "</tr>";
	}
	echo "</table>";

	pg_close($conn);
?>
</body>
</html>